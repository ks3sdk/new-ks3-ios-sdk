Pod::Spec.new do |s|

  s.name         = "iOS-KS3SDK"
  s.version      = "1.0.7"
  s.summary      = "iOS SDK for Kingsoft Standard Storage Service"

  s.description  = <<-DESC
    An iOS SDK for developers to use Kingsoft Standard Storage Service easier.
                   DESC

  s.homepage     = "https://gitee.com/ks3sdk/new-ks3-ios-sdk"

  s.license      = "Apache License, Version 2.0"

  s.author       = { "cqc" => "cqclife@qq.com" }

  s.source       = { :git => "https://gitee.com/ks3sdk/new-ks3-ios-sdk.git", :tag => "v#{s.version}" }

  s.requires_arc = true

  s.ios.deployment_target = '11.0'

  s.source_files  = "KS3YunSDK/**/*.{h,m}"
  
  s.user_target_xcconfig = {
        'GENERATE_INFOPLIST_FILE' => 'YES'
    }
  s.pod_target_xcconfig = {
        'GENERATE_INFOPLIST_FILE' => 'YES'
    }
end
